---
author: MECY
title: Welcome
---



??? info  site en construction
     Pour le moment, les liens pointent vers les activités Capytale, dont l'accès est limité à mes élèves. Les pages seront publiées ici en fonction de l'avancée de mes élèves.


😊  Bienvenue !



# Progression 1ERE NSI 2024-2025

[Qu'est-ce qu'un notebook ?](https://capytale2.ac-paris.fr/web/c/5db4-4121055)


## Partie 1 : Programmons

L'ensemble de cette partie est consacré à l'apprentissage de Python. Nous l'abordons tôt, car cela constitue un outil puissant pour travailler sur le reste du programme.

### Chapitre 1 : Premiers pas en Python

Ce chapitre permet d'aborder les notions suivantes :
- Variables et Affectations
- Opérateurs
- [Structure Conditionnelle](https://capytale2.ac-paris.fr/web/c/eb99-4121660)

La première partie de ce cours n'a pas fait l'objet d'un notebook dédié, mais [quelques révisions](https://capytale2.ac-paris.fr/web/c/72b0-4121091) ont été faites sous cette forme et des [règles de nommage des variables](https://capytale2.ac-paris.fr/web/c/5f42-4121759) ont été posées.

Les exercices correspondants sont ici:
- [exercices : les conditions](https://capytale2.ac-paris.fr/web/c/c871-3970802) - [corrections](https://capytale2.ac-paris.fr/web/c/0738-4130974)

Durant ce chapitre nousavons également abordé turtle pour mettre application les concepts rencontrés à travers une fiche d'exercices: 

- [exercices : turtle 1](https://capytale2.ac-paris.fr/web/c/bab3-4121499)  -  [corrections](https://capytale2.ac-paris.fr/web/c/4509-4124376)



!!! info
    À la fin de ce chapitre, nous savons écrire une séquence d'instructions. On parle de programmation séquentielle.


### Chapitre 2 : Structurons notre code

Ce chapitre se concentre sur :
- [la boucle for](https://capytale2.ac-paris.fr/web/c/6add-4008271)  
- [la boucle while](https://capytale2.ac-paris.fr/web/c/e562-4026211)
- [les fonctions](https://capytale2.ac-paris.fr/web/c/8ed2-4075439)

Les exercices correspondants sont ici:

- [exercices : boucle for](https://capytale2.ac-paris.fr/web/c/d3e6-3970671)  -  [corrections](https://capytale2.ac-paris.fr/web/c/6025-4126717)
- [exercices : boucle while](https://capytale2.ac-paris.fr/web/c/b37b-4027786) - [corrections](https://capytale2.ac-paris.fr/web/c/a0ac-4131094)
- [exercices : les fonctions](https://capytale2.ac-paris.fr/web/c/9e76-4146635) - corrections  (à venir. Ne soyez pas si pressés ;) )

!!! info 
    À la fin de ce chapitre, nous savons gérer les répétitions de blocs de code, définir et appeler des fonctions. C'est la programmation procédurale.


